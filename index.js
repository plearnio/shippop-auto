const express = require("express");
const axios = require("axios");
const xlsx = require("node-xlsx");
const fs = require("fs");
const { default: Axios } = require("axios");
const app = express();

const port = 3000;

app.get("/", (req, res) => {
  const workSheetsFromBuffer = xlsx.parse(
    fs.readFileSync(`${__dirname}/shipment_test.xlsx`)
  );
  const workSheetsFromFile = xlsx.parse(`${__dirname}/shipment_test_all.xlsx`);
  const outputShipments = {
    shipments: [],
  };
  workSheetsFromFile[0].data[0] = 'teststest'
  // xlsx.build({ name: `${__dirname}/shipment_test.xlsx`, data: workSheetsFromFile })
  workSheetsFromFile[0].data.shift();
  const outputShipment = [];
  workSheetsFromFile[0].data = workSheetsFromFile[0].data.filter((data) => {
    return data.length > 0;
  });

  const dataOutput = workSheetsFromFile[0].data.map((data, index) => {
    const typeShipment = ["parcel", "document"];
    typeShipment.forEach((typeSm) => {
      const widthShipment = [1, 10, 100];
      const weightShipment = [500, 30000];
      widthShipment.forEach((widthSm) => {
        weightShipment.forEach((weightSm) => {
          const addressSplit = data[36] ? data[36].split(/(.{1,50})(?:\s|$)|(.{50})/).filter((str) => str && str.length > 0) : ''
          let address_1 = addressSplit[0]
          let address_2 = ''
          if (addressSplit.length > 0) {
            address_2 = addressSplit[1]
          }

          outputShipment.push({
            _row: index,
            type: typeSm,
            width: typeSm === 'document' ? 1 : widthSm,
            length: typeSm === 'document' ? 1 : widthSm,
            height: typeSm === 'document' ? 1 : widthSm,
            total_weight: weightSm,
            remark: "Automation test",
            require_coverage: false,
            origin_address: {
              name: `Pruek`,
              company: "",
              phone: `+66-950063706`,
              address: `Phayathai Plaza, Phayathai`,
              address2: "",
              address3: "",
              state: "mochit",
              city: "bangkok",
              postcode: "10600",
            },
            destination_address: {
              name: data[32],
              company: "",
              phone: `+${data[34]}-${data[35].toString().replace(/[^a-zA-Z0-9]/g, "")}`,
              address: address_1,
              address2: address_2,
              address3: "",
              state: data[42] || '',
              city: data[43],
              postcode: data[38],
              country_code: data[41] || "",
            },

            goods: typeSm === 'document' ? [{
              name: 'Document',
              pieces: 1,
              weight: 100,
              price: 10,
              currency: "THB",
              sku_number: "",
              hs_code: "",
              manufacturer_country_code: "TH",
            }] : [
              {
                name: data[8],
                pieces: 1,
                weight: 100,
                price: 10,
                currency: data[23],
                sku_number: "",
                hs_code: "",
                manufacturer_country_code: data[15] || "TH",
              },
              {
                name: data[9],
                pieces: 1,
                weight: 100,
                price: 100,
                currency: data[23],
                sku_number: "",
                hs_code: "",
                manufacturer_country_code: data[16] ||  "TH",
              },
              {
                name: data[10],
                pieces: 1,
                weight: 100,
                price: 1000,
                currency: data[23],
                sku_number: "",
                hs_code: "",
                manufacturer_country_code: data[17] ||  "TH",
              },
            ],
          });
        });
      });
    });
  });

  const testOneShipment = outputShipment[0];
  const responseList = [];

  const fetch = (obj, _row) =>
    new Promise((resolve) =>
      setTimeout(
        () => {
          axios(obj)
            .then((data = {}) => {
              fs.appendFileSync(
                "./result",
                `[row = ${_row}], data: ${JSON.stringify(data.data)}\r\n`,
                { flags: "a" }
              );
              console.log(`[row = ${_row}], data: `, data.data);
              resolve();
            })
            .catch((err) => {
              const errResponse = err.response
                ? err.response.data
                : err.message;
              fs.appendFileSync(
                "./result",
                `[row = ${_row}], data ERROR: ${JSON.stringify(
                  errResponse
                )}\r\n`,
                { flags: "a" }
              );
              console.log(
                `[row = ${_row}], data ERROR: `,
                err.response ? err.response.data : err.message
              );
              resolve();
            });
        },
        3000,
        obj
      )
    );

  const addTask = (() => {
    let pending = Promise.resolve();

    const run = async (outputReq) => {
      try {
        await pending;
      } finally {
        // console.log(`[row = ${outputReq._row}], request`, outputReq)
        fs.appendFileSync(
          "./result",
          `[row = ${outputReq._row}], request: ${JSON.stringify(
            outputReq
          )}\r\n`,
        );
        return fetch(
          {
            url: "https://staging-inter.shippop.com/api/platform/shipment",
            method: "POST",
            data: {
              shipment: outputReq,
            },
            headers: {
              Authorization:
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoyNSwibWVtX2lkIjoiQy0wMDAwMjUiLCJuYW1lIjoiUHJ1ZWsgIFBha2FtYXRhd2VlIiwiZW1haWwiOiJwLnBsZWFybi5pb0BnbWFpbC5jb20iLCJjb25maXJtZWRfZW1haWwiOiIyMDIwLTA2LTAyVDA5OjIwOjUyLjAwMFoiLCJzdG9yZV9uYW1lIjpudWxsLCJwaG9uZSI6bnVsbCwiY291bnRyeSI6IlRIIiwiYWN0aXZhdGVkIjoiMjAyMC0wNi0wMlQwOToyMDo1Mi4wMDBaIiwiZmFjZWJvb2tfaWQiOiIxMDIxNjkwNTI4ODM3MzMwOCJ9LCJpYXQiOjE1OTQ4NzIwOTF9.J7DiTznoGVZmPLSyy6_6IW5WAayNW0X9BLTB1UsEbnA",
            },
          },
          outputReq._row
        );
      }
    };

    return (outputReq) => (pending = run(outputReq));
  })();

  for (let i = 0; i < outputShipment.length; i += 1) {
    // console.log(outputShipment[i])
    addTask(outputShipment[i]);
  }
  res.send("start");
});

app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);
